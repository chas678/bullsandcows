package com.pobox.cbarham;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Scanner;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;


public class BullCowTest {
    private Random rnd;
    private BullCow sut;

    @Before
    public void onSetup() {
        Scanner scanner = new Scanner("9876");  // simulated input
        rnd = new Random(123123L); // constant seed for predictable random nums
        sut = new BullCow(rnd, scanner);
    }

    @Test
    public void shouldGenerate4NumbersFrom1To9() throws Exception {
        assertThat(sut.get4RandomUniqueNumbers(), hasSize(4));
        assertThat(sut.get4RandomUniqueNumbers(), contains(9, 7, 8, 3));
    }

    @Test
    public void shouldGenerate4UniqueNumbersFrom1To9() {
        for (int i = 0; i <= 10000; i++) {
            // Sets have no duplicates, so if a number appears twice then size would be less than 4
            assertThat(new HashSet<>(sut.get4RandomUniqueNumbers()), hasSize(4));
        }
    }

    @Test
    public void shouldAcceptGoodNumberInput() {
        Optional<Integer> result = sut.readA4DigitNumber();
        assertThat(result.isPresent(), is(true));
        assertThat(result.get(), is(9876));
    }

    @Test
    public void shouldRejectNonNumberInput() {
        Scanner scanner = new Scanner("abcd");
        sut = new BullCow(rnd, scanner);
        Optional<Integer> result = sut.readA4DigitNumber();
        assertThat(result.isPresent(), is(false));
    }

    @Test
    public void shouldRejectNoInput() {
        Scanner scanner = new Scanner("");
        sut = new BullCow(rnd, scanner);
        Optional<Integer> result = sut.readA4DigitNumber();
        assertThat(result.isPresent(), is(false));
    }

    @Test
    public void shouldRejectNonNumberInputAndThenAllowsAnotherTry() {
        // test to ensure we advance the scanner following non integer input
        Scanner scanner = new Scanner("abcd\n1234");
        sut = new BullCow(rnd, scanner);
        Optional<Integer> result = sut.readA4DigitNumber();
        assertThat(result.isPresent(), is(false));
        result = sut.readA4DigitNumber();
        assertThat(result.isPresent(), is(true));
    }

    @Test
    public void shouldRejectLowNumberInput() {
        Scanner scanner = new Scanner("999");
        sut = new BullCow(rnd, scanner);
        Optional<Integer> result = sut.readA4DigitNumber();
        assertThat(result.isPresent(), is(false));
    }

    @Test
    public void shouldRejectHighNumberInput() {
        Scanner scanner = new Scanner("10000");
        sut = new BullCow(rnd, scanner);
        Optional<Integer> result = sut.readA4DigitNumber();
        assertThat(result.isPresent(), is(false));
    }

    @Test
    public void shouldRejectDuplicateDigitsInNumberInput() {
        Scanner scanner = new Scanner("1189");
        sut = new BullCow(rnd, scanner);
        Optional<Integer> result = sut.readA4DigitNumber();
        assertThat(result.isPresent(), is(false));
    }


    @Test
    public void testScoreBulls2Cows2() {
        List<Integer> secretNumbers = Arrays.asList(1, 9, 8, 7);
        List<Integer> guess = Arrays.asList(1, 7, 8, 9);
        BullCow.BullsAndCowsScore score = sut.computeScore(secretNumbers, guess);
        assertThat("wrong bulls", score.bulls, is(2));
        assertThat("wrong cows", score.cows, is(2));
    }

    @Test
    public void testScoreBulls1Cows3() {
        List<Integer> secretNumbers = Arrays.asList(1, 9, 8, 7);
        List<Integer> guess = Arrays.asList(1, 7, 9, 8);
        BullCow.BullsAndCowsScore score = sut.computeScore(secretNumbers, guess);
        assertThat("wrong bulls", score.bulls, is(1));
        assertThat("wrong cows", score.cows, is(3));
    }

    @Test
    public void testScoreZero() {
        List<Integer> secretNumbers = Arrays.asList(1, 2, 3, 4);
        List<Integer> guess = Arrays.asList(6, 7, 8, 9);
        BullCow.BullsAndCowsScore score = sut.computeScore(secretNumbers, guess);
        assertThat("wrong bulls", score.bulls, is(0));
        assertThat("wrong cows", score.cows, is(0));
    }

    @Test
    public void testScore4Bulls() {
        List<Integer> secretNumbers = Arrays.asList(1, 2, 3, 4);
        List<Integer> guess = Arrays.asList(1, 2, 3, 4);
        BullCow.BullsAndCowsScore score = sut.computeScore(secretNumbers, guess);
        assertThat("wrong bulls", score.bulls, is(4));
        assertThat("wrong cows", score.cows, is(0));
    }

    @Test
    public void testScore4Cows() {
        List<Integer> secretNumbers = Arrays.asList(1, 2, 3, 4);
        List<Integer> guess = Arrays.asList(4, 3, 2, 1);
        BullCow.BullsAndCowsScore score = sut.computeScore(secretNumbers, guess);
        assertThat("wrong bulls", score.bulls, is(0));
        assertThat("wrong cows", score.cows, is(4));
    }

    @Test
    public void shouldWinGameOnFirstGuess() {
        int guesses = sut.playGame(Arrays.asList(9, 8, 7, 6));
        assertThat(guesses, is(1));
    }

    @Test
    public void shouldWinGameOnThirdGuess() {
        Scanner scanner = new Scanner("1234\n5678\n9876");
        sut = new BullCow(rnd, scanner);
        int guesses = sut.playGame(Arrays.asList(9, 8, 7, 6));
        assertThat(guesses, is(3));
    }
}
