package com.pobox.cbarham;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

class BullCow {
    private Random rnd;
    private Scanner scanner;

    private BullCow() {
        this(new Random(System.currentTimeMillis()), new Scanner(System.in));
    }

    BullCow(Random rnd, Scanner scanner) {
        if (rnd == null) {
            throw new IllegalArgumentException("Need a Random object");
        }
        if (scanner == null) {
            throw new IllegalArgumentException("Need a Scanner object");
        }
        this.rnd = rnd;
        this.scanner = scanner;
    }

    public static void main(String... args) {
        BullCow app = new BullCow();
        int guesses = app.playGame(app.get4RandomUniqueNumbers());
        System.out.println("You won after " + guesses + " guesses!");
    }

    int playGame(List<Integer> randomUniqueNumbers) {
        int guesses = 0;
        List<Integer> guess = new ArrayList<>();
        do {
            Optional<Integer> inputNumbers = readA4DigitNumber();
            if (!inputNumbers.isPresent()) {
                continue;
            }
            guesses++;
            guess = convertNumberToIntegerList(inputNumbers.get());
            BullsAndCowsScore score = computeScore(randomUniqueNumbers, guess);
            if (score.bulls != 4) {
                System.out.println(score.cows + " Cows and " + score.bulls + " Bulls.");
            }
        } while (!guess.equals(randomUniqueNumbers));
        return guesses;
    }

    BullsAndCowsScore computeScore(List<Integer> randomUniqueNumbers, List<Integer> guess) {
        BullsAndCowsScore score = new BullsAndCowsScore();
        for (int i = 0; i < 4; i++) {
            if (guess.get(i).equals(randomUniqueNumbers.get(i))) {
                score.bulls++;
            } else if (randomUniqueNumbers.contains(guess.get(i))) {
                score.cows++;
            }
        }
        return score;
    }

    List<Integer> get4RandomUniqueNumbers() {
        List<Integer> allNumbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        Collections.shuffle(allNumbers, rnd);
        return allNumbers.subList(0, 4);
    }

    Optional<Integer> readA4DigitNumber() {
        Optional<Integer> input = Optional.empty();
        System.out.print("Guess a 4-digit number with no duplicate digits: ");
        try {
            int num = scanner.nextInt();
            if (num < 1000 || num > 9999 || hasDuplicateDigits(num)) {
                printError();
            } else {
                input = Optional.of(num);
            }
        } catch (NoSuchElementException ignored) {
            printError();
            if (scanner.hasNext()) {
                scanner.next();
            }
        }
        return input;
    }

    private boolean hasDuplicateDigits(int num) {
        Set<Integer> set = new HashSet<>(convertNumberToIntegerList(num));
        return set.size() != 4;
    }

    private List<Integer> convertNumberToIntegerList(int num) {
        List<Integer> list = new LinkedList<>();
        char[] digitsArray = Integer.toString(num).toCharArray();
        for (char c : digitsArray) {
            list.add(Integer.parseInt(String.valueOf(c)));
        }
        return list;
    }

    private void printError() {
        System.out.println("Enter a 4-digit number with no duplicate digits, your input was not " +
                "acceptable");
    }

    static class BullsAndCowsScore {
        int bulls;
        int cows;

        BullsAndCowsScore() {
            bulls = 0;
            cows = 0;
        }
    }
}
